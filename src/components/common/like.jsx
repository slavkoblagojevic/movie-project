import React, { Component } from "react";

class Like extends Component {
  handleHeart() {
    let classes = "fa fa-heart";
    classes += this.props.liked === true ? "-o" : "";
    return classes;
  }

  render() {
    return (
      <i
        onClick={this.props.onClick}
        style={{ cursor: "pointer" }}
        className={this.handleHeart()}
      />
    );
  }
}

export default Like;
