import React from "react";

const Pagination = (props) => {
  console.log(props.currentPage);

  const itemsCount = props.itemsCount;
  const pageSize = props.pageSize;
  const pageNumber = Math.ceil(itemsCount / pageSize);

  const pages = [];

  for (let i = 0; i < pageNumber; i++) {
    pages[i] = i + 1;
  }

  if (pages.length < 2) return null;
  return (
    <nav>
      <ul className="pagination">
        {pages.map((page) => (
          <li
            key={page}
            className={
              page === props.currentPage ? "page-item active" : "page-item"
            }
          >
            <a className="page-link" onClick={() => props.onPageChange(page)}>
              {page}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;
