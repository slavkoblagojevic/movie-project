import _ from 'lodash'
//paginatge(movies, currentPage, pageSize)
export default function paginate(items, pageNumber, pageSize){
 const startIndex = (pageNumber - 1) * pageSize
  return _(items).slice(startIndex).take(pageSize).value()
}